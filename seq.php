<?php

//function param($p) { return isset($_GET[$p])?$_GET[$p]:NULL; }
function param($p) { return $_SERVER["QUERY_STRING"]; }

$file = param('QUERY_STRING');
$gz_file = $file . '.gz';
if ( file_exists($gz_file) and preg_match('/\.scafSeq/', $file) ) {
  header('Content-Description: File Transfer');
  header('Content-Type: application/gzip');
  header('Content-Disposition: attachment; filename="'.$gz_file.'"');
  header('Content-Transfer-Encoding: binary');
  header('Content-Length: '.file_size($gz_file));
  header('X-Sendfile: '.$gz_file);

  $fp = fopen($gz_file);
  session_write_close();
  ob_end_clean();
  fpassthru($fp);
} else {
  echo "Not found " . $file;
}
  


