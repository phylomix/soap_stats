<?php
// a php script for NGS assembly report of SOAPdenovo2.
// Toshinori Endo 2017.5.12

$scafStat = glob('*.scafStatistics');
//$scafSeq =  glob('*.scafSeq');

echo "<ol>";
foreach ($scafStat AS $stat) {
  $stem = preg_replace('/Statistics$/', '', $stat);
  $seq  = $stem.'Seq';
  $N50  = `grep N50 $stat | head -1`;
  echo "<li>$stem ( $N50) ";
  echo "<a href='javascript:stat(\"$stat\")'>Stat</a> ";
  echo "<a href='seq.php?$seq'>Seq</a>";
  echo "</li>";
}
echo "</ol>";
echo "K* indicates K-mer size used in assembly step.";
echo "<hr/>";
echo "<span id='content'></span>";
?>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

<script>
function stat($file) {
  $("#content").text($file);
  $.ajax({
    url: "stat.php?" + $file
  })
  .done(function(data) {
      $("#content").html(data);
  });
}

</script>
